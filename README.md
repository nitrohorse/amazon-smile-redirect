# Amazon Smile Redirect

[![devDependency Status](https://david-dm.org/nitrohorse/amazon-smile-redirect/dev-status.svg)](https://david-dm.org/nitrohorse/amazon-smile-redirect?type=dev)
[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/amazon-smile-redirect/blob/master/LICENSE)
[![Downloads](https://img.shields.io/amo/users/amazon-smile-redirector.svg)](https://addons.mozilla.org/en-US/firefox/addon/amazon-smile-redirector/)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

Browser extension that redirects Amazon.com, Amazon.de, and Amazon.co.uk links to Smile.Amazon.* so that you don't forget to contribute to your favorite non-profit ❤

## Download
* [Firefox](https://addons.mozilla.org/en-US/firefox/addon/amazon-smile-redirector/)

## Install Locally
* Firefox: [temporary](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)
* Chrome: [permanent](https://superuser.com/questions/247651/how-does-one-install-an-extension-for-chrome-browser-from-the-local-file-system/247654#247654)

## Develop Locally
* Clone the repo and `cd` into it
* Install tools:
	* [Node.js](https://nodejs.org/en/)
	* [Yarn](https://yarnpkg.com/en/)
* Install dependencies: 
	* `yarn`
* Run add-on in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext):
	* `yarn serve`
* Bundle add-on into a zip file for distribution:
	* `yarn bundle`

## Resources
* Icon by [Icons8](https://icons8.com/)
